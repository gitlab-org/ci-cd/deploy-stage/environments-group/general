#!/usr/bin/env ruby

require 'docopt'
require 'gitlab'
require 'uri'

lib = File.expand_path("../../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'api'
require 'data_selector'
require 'issue_entry'
require 'status_report'

docstring = <<DOCOPT
POST a comment about the current milestone's environment group's issues in their current planning issue.

Usage:
  #{__FILE__} [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help        Show this screen.
  --dry-run        Print the comment body, but do not POST.

DOCOPT

begin
  doc_options = Docopt::docopt(docstring)

  dry_run = doc_options.fetch('--dry-run', false)

  api = Api.new

  data_selector = DataSelector.new

  all_milestones = api.get_milestones

  puts "Determining current milestone..."

  current_milestones = data_selector.current_milestones(all_milestones: all_milestones)

  # We should be left with one milestone which should be our current one. If
  # we can't determine just 1 current milestone, exit.
  return unless current_milestones.count == 1

  current_milestone = current_milestones.first

  issues = api.get_group_issues(current_milestone: current_milestone)

  general_project_issues = api.get_general_project_issues

  current_planning_issues = data_selector.current_planning_issues(
    general_project_issues: general_project_issues,
    title: current_milestone.title
  )

  # We should be left with one planning issue which should be our current
  # one. If we can't determine just 1 current planning issue, exit.
  return unless current_planning_issues.count == 1
  current_planning_issue = current_planning_issues.first

  puts "Determined #{current_planning_issue.web_url} as current planning issue."

  current_planning_issue_comments = api.get_current_planning_issue_comments(
    current_planning_issue_iid: current_planning_issue.iid
  )

  last_posted_bot_comment = data_selector.last_posted_bot_comment(
    current_planning_issue_comments: current_planning_issue_comments
  )

  status_report = StatusReport.new(
    last_posted_bot_comment: last_posted_bot_comment,
    issues: issues
  )

  status_report.prepare_comment_body!

  puts status_report.comment_body

  if dry_run
    puts "You asked for a dry run, so we're not posting any data."
    puts "Dry run complete."
  else
    puts "Posting comment..."
    api.post_comment(
      current_planning_issue_iid: current_planning_issue.iid,
      body: status_report.comment_body
    )
    puts "Finished posting comment"
  end
rescue Docopt::Exit => e
  puts e.message
  exit 1
end
