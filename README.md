# General

All discussions about the organization of the teams within the Deploy:Environments group.

Discussions related to Product development must happen in [gitlab](https://gitlab.com/gitlab-org/gitlab) issue trackers.