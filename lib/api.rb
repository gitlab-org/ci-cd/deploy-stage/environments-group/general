# This class uses the Gitlab client provided by the 'gitlab' gem. Since the
# request commands can get a bit long, the goal is to make them easier to
# reason about by naming methods, along with storing Configure team specific
# ids as constanst instead of loose variables in a script.
#
class Api
  attr_reader :client, :doc_options

  ENDPOINT = 'https://gitlab.com/api/v4'
  GITLAB_GROUP_ID = 9970
  # This is the Environments group General project
  GENERAL_PROJECT_ID = 44140983
  GROUP_LABEL = 'group::environments'

  def initialize
    @client = Gitlab.client(
      endpoint: ENDPOINT,
      private_token: ENV['MILESTONE_UPDATER_TOKEN']
    )
  end

  def get_milestones
    puts "Getting milestones..."
    client.get("/groups/#{GITLAB_GROUP_ID}/milestones").auto_paginate
  end

  def get_group_issues(current_milestone:)
    puts "Getting current milestone issues with label #{GROUP_LABEL}..."

    client.get(
      "/issues?scope=all&milestone=#{current_milestone.title}&labels=#{GROUP_LABEL}&order_by=relative_position&sort=asc"
    ).auto_paginate
  end

  def get_general_project_issues
    puts "Getting Planning Issues for Project  #{GENERAL_PROJECT_ID}..."

    client.get(
      "/projects/#{GENERAL_PROJECT_ID}/issues?labels=Planning Issue"
    ).auto_paginate
  end

  def get_current_planning_issue_comments(current_planning_issue_iid:)
    client.get(
      "/projects/#{GENERAL_PROJECT_ID}/issues/#{current_planning_issue_iid}/notes"
    ).auto_paginate
  end

  def post_comment(current_planning_issue_iid:, body:)
    client.post(
      "/projects/#{GENERAL_PROJECT_ID}/issues/#{current_planning_issue_iid}/notes",
      body: { body: body }
    )
  end
end
