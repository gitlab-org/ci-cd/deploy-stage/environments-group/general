require 'date'

# This class does not make api calls, but rather handles parsing through the
# returned data.
#
class DataSelector
  BOT_USERNAME = "project_44140983_bot_045b6d83f14b953237223ae6529ea3b7"

  def initialize; end

  def current_milestones(all_milestones:)
    # First, filter out past due milestones.
    not_past_due = all_milestones.select do |milestone|
      milestone.due_date && (Date.parse(milestone.due_date) >= Date.today)
    end

    # Using this new dataset, filter out any future, unstarted milestones. This
    # gives us a set of current_milestones which hopefully has a count of 1.
    not_past_due.select do |milestone|
      milestone.start_date && (Date.parse(milestone.start_date) <= Date.today)
    end
  end

  def current_planning_issues(general_project_issues:, title:)
    general_project_issues.select do |issue|
      issue.title.include?(title)
    end
  end

  def last_posted_bot_comment(current_planning_issue_comments:)
    bot_comments = current_planning_issue_comments.select do |note|
      note.author.username == BOT_USERNAME
    end

    return "" if bot_comments.empty?

    # Sort by date posted since there will be multiple comments from the bot.
    # We always want the most recent one.
    bot_comments.max_by { |comment| comment.created_at }.body
  end
end
