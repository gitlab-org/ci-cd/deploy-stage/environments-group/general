# This class takes an Issue api object and provides methods to format it for
# the eventual comment that the bot will post.
#
class IssueEntry
  attr_reader :issue

  def initialize(issue:)
    @issue = issue
  end

  def format
    "\n   | #{closed} #{link}+ #{deliverable} | #{workflow} | #{assignees} |"
  end

  def link
    issue.references.full
  end

  private

  def closed
    issue.state == "closed" ? "[+Closed+]" : nil
  end

  def assignees
    return "None" if issue.assignees.count < 1
    issue.assignees.map { |assignee| "`@#{assignee.username}`" }.join(", ")
  end

  def workflow
    workflows = issue.labels.select { |l| l.include?("workflow") }.map { |w| "~\"#{w}\"" }.join(", ")
    return "None" if workflows.empty?
    workflows
  end

  def deliverable
    return "~Deliverable" if issue.labels.include?("Deliverable")
  end
end
