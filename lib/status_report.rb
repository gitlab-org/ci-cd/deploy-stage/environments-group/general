# StatusReport
# - compares previous and current data
# - updates the data to show Added/Removed
# - prepares comment_body for POSTing
#
# Added/Removed
#   If an issue is Removed since the last time the bot posted, it will get
#   marked as removed on the current run.
#
#   If an issue was marked as removed on the last bot comment, and it is still
#   removed, it will not appear in the current report.
#
#   If an issue is Added since the last time the bot posted, it will get marked
#   as added on the current run.
#
#   If an issue was marked as added on thte last bot comment, and it is still
#   part of the milestone, it will appear in the current report, but it will no
#   longer be marked as added.
#
class StatusReport
  attr_reader :last_posted_bot_comment, :issue_to_entry_map, :comment_body
  def initialize(last_posted_bot_comment:, issues:)
    @issues = issues

    # Already formatted before posting
    @last_posted_bot_comment = last_posted_bot_comment

    @last_posted_by_line = last_posted_bot_comment.split("\n")

    # This matches on the issue reference. We use the references from the last
    # posted commented for comparison against current references we got from an
    # api request.
    #
    @last_posted_entries = @last_posted_by_line.map do |line|
      line.match(/gitlab.*#+\w*/).to_s
    end.reject(&:empty?)

    set_issue_to_entry_map

    @fresh_entries = issue_to_entry_map.keys
  end

  def prepare_comment_body!
    mark_additions
    mark_removals
    ensure_newlines
    @comment_body = "| Issue | Status | Assignee |\n  | ------ | ------ | ------ |"
    entries = issue_to_entry_map.values
    deliverables = entries.select { |line| line.include?("Deliverable") }
    # Move the deliverables to the front of the array and delete them from
    # their old position. Reverse to preserve order of original GET request.
    deliverables.reverse.map { |d| entries.insert(0, entries.delete(d)) }
    @comment_body << entries.join
    # @comment_body = URI.encode_www_form_component(@comment_body)
    @comment_body
  end

  private

  def set_issue_to_entry_map
    @issue_to_entry_map =  {}
    @issues.map do |issue|
      issue_to_entry_map[issue.references.full] = IssueEntry.new(issue: issue).format
    end
  end

  # A list of all removed entries.
  def removed
    @last_posted_entries.difference(@fresh_entries)
  end

  # A list of all added entries.
  def added
    @fresh_entries.difference(@last_posted_entries)
  end

  # A list of all entries that have been neither added nor removed.
  def remaining
    @fresh_issues.difference(added, removed)
  end

  def mark_additions
    added.map do |addition|
      # idempotence
      next if issue_to_entry_map.values.include?("[-Added-] #{addition}")

      # Modify the existing entry since it will already be there.
      issue_to_entry_map[addition] = issue_to_entry_map[addition].gsub(addition, "[-Added-] #{addition}")
    end
  end

  def mark_removals
    removed.map do |removal|
      # idempotence
      next if issue_to_entry_map.values.include?("[-Removed-] #{removal}")

      old_line = @last_posted_by_line.find { |line| line.include?(removal) }

      # If the old line already showed that it was removed, let's just leave it
      # out of the report since it hasn't been "removed" since then as it was
      # already gone.
      return if old_line.include?("[-Removed-] #{removal}")

      new_line = old_line.gsub(removal, "[-Removed-] #{removal}")

      issue_to_entry_map[removal] = new_line
    end
  end

  def ensure_newlines
    issue_to_entry_map.values.map do |entry|
      entry.prepend("\n") unless entry.start_with?("\n")
    end
  end
end
