## Upgrade K8s Version 1.XX

Upgrading a Kubernetes (k8s) version requires bumping dependencies in many GitLab upstream projects, as well doing some integration tests. Refer below to a proposed workflow to execute this updates and test our features.

### Features Impacted

- Agent CI/CD Workflow
- Agent GitOps Workflow
- Cluster Management Project template
- Auto DevOps (Auto Deploy)

### Steps

1. [test-utils-k3s/k3s-gitlab-ci](https://gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci): add 1.xx to supported `k3s` versions. See [example MR](https://gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/-/merge_requests/23).
1. [k8s-agent-qa](https://gitlab.com/gitlab-org/configure/k8s-agent-qa): bump `test-utils/k3s-gitlab-ci` version. See [example commit](https://gitlab.com/gitlab-org/configure/k8s-agent-qa/-/commit/369bcaef000ef8f7b552a33ec68b040a2be6a4b4).
1. [helm-install-image](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image): bump the `KUBECTL` and `test-utils/k3s-gitlab-ci` k3s version. See [example MR](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/-/merge_requests/79).
1. [auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image):
    1. Bump the `test-utils/k3s-gitlab-ci` version in. See [example MR](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/merge_requests/318).
    1. Bump the `helm-install-image` version to the latest from Step 3.
    1. In the [GitLab project](https://gitlab.com/gitlab-org/gitlab), bump the auto-deploy image in the [Auto Deploy job template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml). Test Auto Deploy.
1. [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications): bump the `helm-install-image` version. See [example MR](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/-/merge_requests/177).
1. [project-templates/cluster-management](https://gitlab.com/gitlab-org/project-templates/cluster-management): make sure that all applications support Kubernetes 1.xx
    - Install all apps in a Kubernetes cluster with the version being tested, and possibly bump the app versions, or do a fix.

### Tips

#### Create a K8s cluster with K3d locally for a specific k8s version

Replace `docker.io/rancher/k3s@sha256:eb1210b559946a35a1877acab4a7a7d91d8389ef000007ef64d6f1a6e478814b` by the [k3s version](https://hub.docker.com/r/rancher/k3s/tags) that has your desired k8s version.

```shell
k3d cluster create gdk-with-registry-1-XX \
  --volume "/Users/your-user/.k3d/my-registries.yaml:/etc/rancher/k3s/registries.yaml" \
  --volume "/Users/your-user/.k3d/registry_host.crt:/etc/ssl/certs/registry_host.crt" \
 --image "docker.io/rancher/k3s@sha256:eb1210b559946a35a1877acab4a7a7d91d8389ef000007ef64d6f1a6e478814b"
```
