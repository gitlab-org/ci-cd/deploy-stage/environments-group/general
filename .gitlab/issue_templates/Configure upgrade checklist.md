Upgrade manager: **`@username`**

## Preamble

- [ ] Set the title of the issue to `X.XX Configure upgrade` checklist
- [ ] Set the milestone of the issue

## Verify the list of K8s deprecations

- [ ] Update the [list of relevant deprecations](https://gitlab.com/gitlab-org/configure/general/-/issues/139), if necessary.
- [ ] Notify managers if a deprecation issue needs to be taken care soon, for its prioritizaion.

## [helm-install-image](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/)

This is the base image for `auto-deploy-image` and `cluster-applications` (but also a public utility image).

- [ ] Upgrade `kubectl` minor/patch version. Remember to review the relevant
      [changelog](https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG)
      for breaking changes.
  - We upgrade `kubectl` based on the oldest
    [kubernetes version that we support](https://docs.gitlab.com/ee/user/project/clusters/#supported-cluster-versions).
    Due to the [kubernetes version skew policy](https://kubernetes.io/docs/setup/release/version-skew-policy/#kubectl),
    we should always use *one minor version above our oldest supported version*.
- [ ] Upgrade `helm` binary
  - [ ] Review [helm releases](https://github.com/helm/helm/releases) for breaking changes

## [auto-build-image](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image)

Used by the Build stage of Auto DevOps. 

- [ ] Update the pinned `DOCKER_VERSION` in [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/master/.gitlab-ci.yml). This should be done in sync with the [Auto DevOps updates](#auto-devops)
- [ ] Update the pinned [pack CLI](https://buildpacks.io/docs/install-pack/#linux) version in the [`Dockerfile`](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/master/Dockerfile)
- [ ] Update the pinned `buildx` version in the [`Dockerfile`](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/master/Dockerfile)

## [auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image)

Used by the Deploy stages of Auto DevOps

- [ ] Upgrade to use latest version of `helm-install-image`
- [ ] Upgrade `glibc` - Check https://github.com/sgerrand/alpine-pkg-glibc
- [ ] Bump the chart version in `Chart.yaml`
  - [ ] Ask a maintainer to tag the commit for that version bump with the version. This will trigger a release job for the version

## Auto DevOps

Each in an MR of its own:

- [ ] Update the pinned version of the docker build images `docker:X.Y.Z-dind` and `docker:X.Y.Z` (see [`docker` on Dockerhub](https://hub.docker.com/_/docker/) for the latest tags):
    * [Build template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml),
    * [Browser Performance Testing template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml)
    * [Code Quality template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml)
- [ ] Update [Deploy sub-template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml) to use latest version of `auto-deploy-image`. 

*You'll be tempted to create a branch name with the `auto-deploy` character sequence. Branches with this sequence are automatically set as protected branches, so if you're not maintainer, you're probably not going to be able to push this branch to the remote repository. So, just choose another branch name.*

- [ ] Update [DAST-Default-Branch-Deploy template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml) to use latest version of `auto-deploy-image`

**Important:** Please run QA pipelines on each MR.

## [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications)

- [ ] Update [`helm-install-image`](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image)
- [ ] Update [`helmfile`](https://github.com/roboll/helmfile/releases)
- [ ] Update [`helm-git`](https://github.com/aslafy-z/helm-git/releases)

## [Cluster Management Project Template](https://gitlab.com/gitlab-org/project-templates/cluster-management/)

- [ ] Bump `cluster-applications` in `.gitLab-ci.yml`
- [ ] Update charts:
  - [ ] Ingress
  - [ ] Cert-manager
- [ ] [Update the vendored template](https://docs.gitlab.com/ee/development/rake_tasks.html#update-project-templates) in `gitlab-org/gitlab`

## Administration

- [ ] Before, closing this issue, create next month's issue using template `Configure upgrade checklist`


/label ~"group::configure" ~"devops::configure" ~"dependency update"
