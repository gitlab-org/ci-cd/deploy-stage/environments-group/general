## <Feature to remove>

## Proposal

<!-- Write this following the checklist below and common sense -->

## Issue readiness checklist

- [ ] This issue/epic is shared with the team at a team meeting
- [ ] The proposal clearly states what is being deprecated, mentioning potentially related areas and features that are not affected
- [ ] The (in-product) communication plan about the removal is described in the proposal
- [ ] The documentation requirements of the removal are described in the proposal
- [ ] A preliminary timeline for the deprecation and removal is described in the proposal
- [ ] Deprecation message added to the `CHANGELOG.md` file: https://gitlab.com/gitlab-org/gitlab/merge_requests/XXX
- [ ] Deprecation message posted in the release blog post: https://gitlab.com/gitlab-com/www-gitlab-org/merge_requests/XXX
- [ ] Deprecated code removed in %"15.0": https://gitlab.com/gitlab-org/gitlab/merge_requests/XXX

/label ~"group::configure" ~deprecation 
/milestone %"15.0" 
/epic https://gitlab.com/groups/gitlab-org/-/epics/6940
