<!-- Title: <group> <xy.z milestone> Planning issue -->

## Milestone dates

- Milestone starts (Friday before the 3rd Thursday):
- Code cut-off: (Friday before the 3rd Thursday):
- [Milestone ends](https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/) (3rd Thursday of the month):

## Direction and direction changes
<!-- Summarise important achievement reached and eventual direction changes/focus shift -->

## Goal

1. goal 1
1. goal 2

### Due bugs

[Link to a list of S1-S3 bugs ordered by due date.](https://gitlab.com/groups/gitlab-org/-/issues/?sort=due_date&state=opened&label_name%5B%5D=group%3A%3Aenvironments&label_name%5B%5D=type%3A%3Abug&not%5Blabel_name%5D%5B%5D=severity%3A%3A4&not%5Blabel_name%5D%5B%5D=Category%3ATerraform%20provider&first_page_size=20)

## Quarterly OKR status

<!-- Summarize the status of OKRs here -->

## The Roadmap

The roadmap is maintained on [the Delivery direction page](https://about.gitlab.com/direction/delivery/).

## Capacity planning

Check our [PTO Calendar Epic](https://gitlab.com/groups/gitlab-org/ci-cd/deploy-stage/environments-group/-/epics/3) (see the roadmap view)

@nmezzopera to call out any important Capacity variation

## The Board

As with any board in GitLab an issue might appear in multiple columns, we try to avoid this here. As a result originally UX issues might have lost the UX label already, if they are ready for dev work.
The Deliverable and Stretch columns are priority ordered. Let's hope/prey that the ordering won't change unexpectedly.
If you remove something from Deliverable or Stretch, or move an issue between these, please mention in here in a comment and at least mention @nagyv-gitlab to know about it

[Planning board](https://gitlab.com/groups/gitlab-org/-/boards/2395842) 

## UX Plan

<!-- Summarize UX Plan for the milestone here -->

| Issue | Weight | Note |
|-------|--------|------|
| Issue link | Design weight | Note |


## Tracking

```glql
---
display: table
fields: title, state, healthStatus, epic, weight, labels("workflow::*"), assignee
limit: 40
---
milestone = "17.7" AND label = "group::environments" AND group = "gitlab-org" 
```

## Actions

Close issue if all these are done:

* [ ] (optional) Ping Product Leader on MVC, Tiering, and New Config Review topics is needed
* [ ] once the team is aligned, set ~"workflow::ready for development" on every issue in ~Deliverable and ~Stretch
* [ ] create the kickoff video with the title `GitLab XX.XX Kickoff - Deploy:Environments`
* [ ] if there is [a kickoff issue](https://gitlab.com/gitlab-com/Product/issues?scope=all&utf8=%E2%9C%93&state=opened&search=kickoff), check it

/label ~"Planning Issue" ~"section::ops"
