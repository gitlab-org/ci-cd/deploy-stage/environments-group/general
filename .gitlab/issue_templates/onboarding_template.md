## Welcome to GitLab and the Environments Team!

We are all excited that you are joining us on the Environments Team.  You should have already received an onboarding issue from People Ops familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Environments Team people, processes and setup.
For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Environments team.
Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day
* [ ] Manager: Invite team member to #g_environments Slack Channel
* [ ] Manager: Invite team member to Weekly Team Meetings
* [ ] Manager: Add new team member to Environments Team Retro

### First two weeks
* [ ] Manager: Add new team member to [geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Introduce new team members in relevant Slack channels
* [ ] Manager: Add new team member to Environments shared calendar
* [ ] New team member: Read about your team, its mission, team members and resources on the [Environments Team page](https://about.gitlab.com/handbook/engineering/development/ops/Environments/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team
* [ ] New team member: One of the items in your main onboarding checklist would be to install the [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit/). Refer to the [guide below](#setting-up-the-gdk-with-relevant-services) for a step-by-step instruction for installing the GDK and other services relevant to the Environments Team.
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
* [ ] New team member: Familiarize yourself with the team boards [Environments Team Workflow](https://gitlab.com/groups/gitlab-org/-/boards/4176401)
* [ ] New team member: Please review this onboarding issue and update [the template](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/blob/master/.gitlab/issue_templates/onboarding_template.md) with some improvements as you see fit to make it easier for the next newbie!
* [ ] New team member: add the Environments shared calendar to Time Off by Deel
  1. In Google Calendar => My Calendars => Hover over `Environments Group Calendar` -> Select `Settings and sharing`
  1. Scroll down to `Integrate calendar` and copy `Calendar ID`
  1. Under Slack -> Time Off by Deel -> Home, click on the dropdown `Your Events` (first in the list) and select `Calendar Sync`
  1. Add Calendar and paste the Calendar ID we copied at the point 2
  1. :tada:
  1. If the `Group Calendar` calendar does not appear in `My Calendars` ping your Engineering Manager in the onboarding issue

### Third Week
* [ ] New team member: Setup and run Auto DevOps
* [ ] New team member: Familiarise yourself with how [to setup a cluster](https://docs.gitlab.com/ee/user/clusters/create/)
* [ ] New team member: Familiarize yourself with our [Terraform integrations](https://docs.gitlab.com/ee/user/infrastructure/)
* [ ] New team member: Familiarize yourself with our [Kubernetes Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent)
* [ ] New team member: Familiarize yourself with [the team repos outlined below](#team-repos)

## Processes
#### Engineering Workflow

* [https://about.gitlab.com/handbook/engineering/workflow/](https://about.gitlab.com/handbook/engineering/workflow/)

#### Important Dates

* https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline

#### Workflow Labels

* https://about.gitlab.com/handbook/product-development-flow/

#### Merge Request Workflow

* https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html

#### Code Review Guidelines

* https://docs.gitlab.com/ee/development/code_review.html

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

* https://docs.gitlab.com/ee/development/feature_flags/
* https://docs.gitlab.com/ee/development/feature_flags/controls.html#rolling-out-changes

#### Testing Best Practices

* https://docs.gitlab.com/ee/development/testing_guide/best_practices.html

## Setting up your local environment / Getting started tasks

To use some of Environments’s product categories in your local environment, like Auto DevOps and Kubernetes Management, you have to set up some extra features in your GDK installation. These guides will help you to get up and running:

### Obtain an EE license for local development
You will need to run the GitLab EE edition on your local environment for full access to Environments team features.
- [ ] Follow the steps to [request a GitLab EE developer license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses)
- [ ] Environments it following the steps described on the [documentation page](https://docs.gitlab.com/ee/user/admin_area/license_file.html)

### Provisioning cloud services with Sandbox Cloud Realm

* [ ] Familiarize yourself with the [Sandbox Cloud Realm](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)

### Setting up the permissions

To be able to run all the services required to work with the feature of the stage you will need to request certain permissions.

* [ ] Find the system/role entries present in this [confidential issue](https://gitlab.com/gitlab-org/configure/onboarding/-/issues/9)
* [ ] Create a [bulk access request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request) using those entries. 
  * Refer to the team members' access requests: [#24042](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/24042), [#18325](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/18325), [#18074](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/18074).
* [ ] Create an [access request for GCP shared infrastructure](https://gitlab.com/gitlab-com/it/infra/issue-tracker/-/issues/new?issuable_template=gcp_group_account_iam_update_request). Its usage is documented [here](https://about.gitlab.com/handbook/engineering/development/ops/deploy/environments/#shared-cloud-infrastructure).
  * Here are examples: [#626](https://gitlab.com/gitlab-com/it/infra/issue-tracker/-/issues/626), [#495](https://gitlab.com/gitlab-com/it/infra/issue-tracker/-/issues/495).

### Setting up the GDK with relevant services

1. [Install GDK with the one-liner command](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation)
1. [Enable NGINX+HTTPS](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/nginx.md)
1. Install Docker. Note that you cannot use Docker Desktop due to licensing reasons. Please refer to [this guide](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop) for alternatives, and feel free to ask your teammates for input or feedback on the alternatives they are using.
1. [Set up the GitLab Runner with Docker configuration](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md)
1. Set up the [GitLab Agent Server](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md). You can ignore the optional steps for now. You can also skip this step until you are ready to test [connecting a project to the GitLab Agent](#connecting-a-project-in-your-local-gitlab-instance-to-a-local-kubernetes-cluster-using-cicd-workflow).

### Connecting a project in your local GitLab instance to a local Kubernetes cluster, using CI/CD workflow

1. If you have not done so, familiarize yourself with the [GitLab Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent), especially the [GitLab Agent (`agentk`) and GitLab Agent Server (`kas`) architecture](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md).
1. If you have not done so, set up the [GitLab Agent Server](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md)
1. [Install gcloud and kubectl](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc/howto/kubernetes#install-gcloud-and-kubectl)
1. Create a project on your local GitLab instance
1. [Register the agent with GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#installation-steps). (You can ignore the prerequisites here since we are using a local Kubernetes distribution.)
1. [Deploy the agent to your local Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md#optional-deploy-the-gitlab-agent-agentk-with-k3d). This example is using `k3d`, but you can use `minikube` or similar alternatives.
1. Make sure that [your runner will be able to communicate with the Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md#optional-connecting-your-project-to-agentk-using-ci-tunnel).

For further reference, review:

* [GDK with a GKE cluster guide](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc/howto/kubernetes) to set up your GDK instance
* [Connecting a Kubernetes cluster with GitLab](https://docs.gitlab.com/ee/user/clusters/agent/)

## Team Repos

There are several projects that the Environments team is primarily responsible for. Feel free to familiarize yourself with the project below:

* [helm-install-image](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/)
* [auto-build-image](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image)
* [auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image)
* [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications)
* [terraform-images](https://gitlab.com/gitlab-org/terraform-images)
* [cluster-management-project](https://gitlab.com/gitlab-org/project-templates/cluster-management/)
* [gitlab-agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/)
* [terraform-provider-gitlab](https://gitlab.com/gitlab-org/terraform-provider-gitlab)
* [k3s-gitlab-ci](https://gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/)

/confidential
/due in 21 days
